//
//  JSONParser.swift
//  Perceptron
//
//  Created by Daniel F. Sampaio on 06/05/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

import Cocoa

class JSONParser: NSObject {
    func parseJSONToArray(jsonString: String) -> NSMutableArray {
        let jsonData: NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding)!
        let jsonArray: NSMutableArray = NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableArray
        return jsonArray
    }
    
    func parseDotaDictToInstancia(dotaDict: Dictionary<String, Int>) -> Instancia {
        return Instancia(valores: [
            Double(dotaDict["deaths"]!),
            Double(dotaDict["denies"]!),
            Double(dotaDict["xp_per_min"]!),
            Double(dotaDict["gold_per_min"]!),
            Double(dotaDict["gold_spent"]!),
            Double(dotaDict["kills"]!),
            Double(dotaDict["tower_damage"]!),
            Double(dotaDict["level"]!),
            Double(dotaDict["match_duration"]!),
            Double(dotaDict["last_hits"]!),
            Double(dotaDict["gold"]!),
            Double(dotaDict["assists"]!),
        ], classe: Double(dotaDict["isCarry"]!))
    }
}