//
//  main.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 08/04/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

import Foundation

func getJSON(filename: String) -> NSString {
    var str = "/Users/lohannpaternocoutinhoferreira/Documents/iOS Developer/perceptron/Perceptron/\(filename)"
    var jsonStr = NSString(contentsOfFile: str, encoding: NSUTF8StringEncoding, error: nil)!
    return jsonStr
}
//let jsonString = "[{\"deaths\":1,\"denies\":0,\"xp_per_min\":260,\"gold_per_min\":385,\"gold_spent\":4025,\"kills\":2,\"tower_damage\":678,\"isCarry\":0,\"level\":7,\"match_duration\":677,\"last_hits\":13,\"gold\":1036,\"assists\":8},{\"deaths\":4,\"denies\":3,\"xp_per_min\":283,\"gold_per_min\":264,\"gold_spent\":4395,\"kills\":2,\"tower_damage\":0,\"isCarry\":0,\"level\":10,\"match_duration\":1145,\"last_hits\":54,\"gold\":3,\"assists\":1},{\"deaths\":0,\"denies\":0,\"xp_per_min\":11,\"gold_per_min\":102,\"gold_spent\":825,\"kills\":0,\"tower_damage\":0,\"isCarry\":0,\"level\":1,\"match_duration\":935,\"last_hits\":1,\"gold\":1,\"assists\":1},{\"deaths\":1,\"denies\":2,\"xp_per_min\":414,\"gold_per_min\":427,\"gold_spent\":9165,\"kills\":4,\"tower_damage\":1263,\"isCarry\":0,\"level\":13,\"match_duration\":1429,\"last_hits\":47,\"gold\":1620,\"assists\":6},{\"deaths\":2,\"denies\":0,\"xp_per_min\":220,\"gold_per_min\":357,\"gold_spent\":3930,\"kills\":6,\"tower_damage\":425,\"isCarry\":0,\"level\":7,\"match_duration\":826,\"last_hits\":16,\"gold\":1590,\"assists\":6},{\"deaths\":3,\"denies\":2,\"xp_per_min\":352,\"gold_per_min\":420,\"gold_spent\":8405,\"kills\":2,\"tower_damage\":1540,\"isCarry\":0,\"level\":12,\"match_duration\":1414,\"last_hits\":64,\"gold\":1221,\"assists\":8},{\"deaths\":0,\"denies\":1,\"xp_per_min\":387,\"gold_per_min\":458,\"gold_spent\":10850,\"kills\":7,\"tower_damage\":325,\"isCarry\":0,\"level\":11,\"match_duration\":1252,\"last_hits\":55,\"gold\":1140,\"assists\":4},{\"deaths\":4,\"denies\":1,\"xp_per_min\":334,\"gold_per_min\":400,\"gold_spent\":7690,\"kills\":4,\"tower_damage\":830,\"isCarry\":0,\"level\":11,\"match_duration\":1427,\"last_hits\":50,\"gold\":1974,\"assists\":11},{\"deaths\":4,\"denies\":3,\"xp_per_min\":368,\"gold_per_min\":357,\"gold_spent\":7790,\"kills\":4,\"tower_damage\":1061,\"isCarry\":0,\"level\":13,\"match_duration\":1669,\"last_hits\":42,\"gold\":2297,\"assists\":6},{\"deaths\":6,\"denies\":1,\"xp_per_min\":479,\"gold_per_min\":428,\"gold_spent\":11230,\"kills\":6,\"tower_damage\":874,\"isCarry\":1,\"level\":15,\"match_duration\":1567,\"last_hits\":47,\"gold\":207,\"assists\":15},{\"deaths\":3,\"denies\":4,\"xp_per_min\":274,\"gold_per_min\":293,\"gold_spent\":3120,\"kills\":2,\"tower_damage\":0,\"isCarry\":0,\"level\":11,\"match_duration\":1324,\"last_hits\":62,\"gold\":1,\"assists\":1},{\"deaths\":4,\"denies\":1,\"xp_per_min\":52,\"gold_per_min\":121,\"gold_spent\":1220,\"kills\":0,\"tower_damage\":0,\"isCarry\":0,\"level\":4,\"match_duration\":1358,\"last_hits\":1,\"gold\":0,\"assists\":1},{\"deaths\":11,\"denies\":3,\"xp_per_min\":763,\"gold_per_min\":611,\"gold_spent\":12745,\"kills\":14,\"tower_damage\":128,\"isCarry\":1,\"level\":20,\"match_duration\":1688,\"last_hits\":104,\"gold\":2144,\"assists\":7},{\"deaths\":3,\"denies\":13,\"xp_per_min\":663,\"gold_per_min\":712,\"gold_spent\":17270,\"kills\":12,\"tower_damage\":901,\"isCarry\":1,\"level\":16,\"match_duration\":1308,\"last_hits\":105,\"gold\":266,\"assists\":8},{\"deaths\":5,\"denies\":6,\"xp_per_min\":450,\"gold_per_min\":366,\"gold_spent\":9460,\"kills\":5,\"tower_damage\":756,\"isCarry\":0,\"level\":15,\"match_duration\":1618,\"last_hits\":21,\"gold\":866,\"assists\":8},{\"deaths\":3,\"denies\":1,\"xp_per_min\":412,\"gold_per_min\":352,\"gold_spent\":8585,\"kills\":5,\"tower_damage\":998,\"isCarry\":0,\"level\":13,\"match_duration\":1364,\"last_hits\":32,\"gold\":269,\"assists\":11},{\"deaths\":4,\"denies\":2,\"xp_per_min\":464,\"gold_per_min\":488,\"gold_spent\":10880,\"kills\":6,\"tower_damage\":2992,\"isCarry\":0,\"level\":16,\"match_duration\":1828,\"last_hits\":96,\"gold\":3192,\"assists\":10},{\"deaths\":13,\"denies\":0,\"xp_per_min\":264,\"gold_per_min\":246,\"gold_spent\":5195,\"kills\":3,\"tower_damage\":0,\"isCarry\":0,\"level\":11,\"match_duration\":1679,\"last_hits\":56,\"gold\":245,\"assists\":8},{\"deaths\":7,\"denies\":1,\"xp_per_min\":285,\"gold_per_min\":270,\"gold_spent\":5515,\"kills\":4,\"tower_damage\":0,\"isCarry\":0,\"level\":12,\"match_duration\":1756,\"last_hits\":40,\"gold\":1067,\"assists\":3},{\"deaths\":8,\"denies\":9,\"xp_per_min\":394,\"gold_per_min\":346,\"gold_spent\":8065,\"kills\":2,\"tower_damage\":0,\"isCarry\":0,\"level\":14,\"match_duration\":1726,\"last_hits\":133,\"gold\":668,\"assists\":3},{\"deaths\":6,\"denies\":2,\"xp_per_min\":289,\"gold_per_min\":239,\"gold_spent\":5160,\"kills\":4,\"tower_damage\":0,\"isCarry\":0,\"level\":13,\"match_duration\":1977,\"last_hits\":48,\"gold\":1999,\"assists\":9},{\"deaths\":9,\"denies\":4,\"xp_per_min\":527,\"gold_per_min\":453,\"gold_spent\":9775,\"kills\":9,\"tower_damage\":0,\"isCarry\":1,\"level\":17,\"match_duration\":1753,\"last_hits\":75,\"gold\":2139,\"assists\":6},{\"deaths\":10,\"denies\":0,\"xp_per_min\":313,\"gold_per_min\":266,\"gold_spent\":6645,\"kills\":6,\"tower_damage\":30,\"isCarry\":0,\"level\":13,\"match_duration\":1761,\"last_hits\":21,\"gold\":803,\"assists\":7},{\"deaths\":7,\"denies\":2,\"xp_per_min\":320,\"gold_per_min\":292,\"gold_spent\":6915,\"kills\":3,\"tower_damage\":45,\"isCarry\":0,\"level\":12,\"match_duration\":1547,\"last_hits\":74,\"gold\":106,\"assists\":3},{\"deaths\":10,\"denies\":3,\"xp_per_min\":335,\"gold_per_min\":298,\"gold_spent\":7400,\"kills\":6,\"tower_damage\":147,\"isCarry\":0,\"level\":13,\"match_duration\":1637,\"last_hits\":55,\"gold\":43,\"assists\":2},{\"deaths\":0,\"denies\":2,\"xp_per_min\":484,\"gold_per_min\":404,\"gold_spent\":4065,\"kills\":4,\"tower_damage\":0,\"isCarry\":1,\"level\":9,\"match_duration\":617,\"last_hits\":25,\"gold\":727,\"assists\":2},{\"deaths\":2,\"denies\":5,\"xp_per_min\":393,\"gold_per_min\":484,\"gold_spent\":12480,\"kills\":13,\"tower_damage\":1790,\"isCarry\":0,\"level\":15,\"match_duration\":1945,\"last_hits\":88,\"gold\":3293,\"assists\":4},{\"deaths\":12,\"denies\":6,\"xp_per_min\":424,\"gold_per_min\":403,\"gold_spent\":13020,\"kills\":6,\"tower_damage\":293,\"isCarry\":1,\"level\":16,\"match_duration\":1938,\"last_hits\":134,\"gold\":47,\"assists\":6},{\"deaths\":1,\"denies\":4,\"xp_per_min\":715,\"gold_per_min\":574,\"gold_spent\":14970,\"kills\":14,\"tower_damage\":3097,\"isCarry\":1,\"level\":20,\"match_duration\":1866,\"last_hits\":106,\"gold\":3799,\"assists\":6},{\"deaths\":7,\"denies\":1,\"xp_per_min\":227,\"gold_per_min\":195,\"gold_spent\":5000,\"kills\":0,\"tower_damage\":0,\"isCarry\":0,\"level\":10,\"match_duration\":1550,\"last_hits\":46,\"gold\":163,\"assists\":1},{\"deaths\":3,\"denies\":3,\"xp_per_min\":255,\"gold_per_min\":308,\"gold_spent\":6705,\"kills\":2,\"tower_damage\":541,\"isCarry\":0,\"level\":11,\"match_duration\":1551,\"last_hits\":32,\"gold\":1331,\"assists\":12},{\"deaths\":8,\"denies\":0,\"xp_per_min\":598,\"gold_per_min\":430,\"gold_spent\":14325,\"kills\":8,\"tower_damage\":355,\"isCarry\":1,\"level\":19,\"match_duration\":2006,\"last_hits\":119,\"gold\":3171,\"assists\":10},{\"deaths\":11,\"denies\":7,\"xp_per_min\":464,\"gold_per_min\":433,\"gold_spent\":8595,\"kills\":5,\"tower_damage\":39,\"isCarry\":1,\"level\":14,\"match_duration\":1437,\"last_hits\":71,\"gold\":499,\"assists\":1},{\"deaths\":12,\"denies\":1,\"xp_per_min\":221,\"gold_per_min\":193,\"gold_spent\":5175,\"kills\":3,\"tower_damage\":0,\"isCarry\":0,\"level\":11,\"match_duration\":1871,\"last_hits\":37,\"gold\":41,\"assists\":2},{\"deaths\":14,\"denies\":2,\"xp_per_min\":311,\"gold_per_min\":256,\"gold_spent\":6940,\"kills\":5,\"tower_damage\":115,\"isCarry\":0,\"level\":14,\"match_duration\":2054,\"last_hits\":55,\"gold\":643,\"assists\":6},{\"deaths\":14,\"denies\":4,\"xp_per_min\":461,\"gold_per_min\":394,\"gold_spent\":10280,\"kills\":5,\"tower_damage\":628,\"isCarry\":1,\"level\":16,\"match_duration\":1829,\"last_hits\":116,\"gold\":114,\"assists\":7},{\"deaths\":6,\"denies\":2,\"xp_per_min\":429,\"gold_per_min\":391,\"gold_spent\":12425,\"kills\":10,\"tower_damage\":1153,\"isCarry\":0,\"level\":17,\"match_duration\":2150,\"last_hits\":58,\"gold\":187,\"assists\":9},{\"deaths\":5,\"denies\":0,\"xp_per_min\":536,\"gold_per_min\":545,\"gold_spent\":12970,\"kills\":17,\"tower_damage\":793,\"isCarry\":1,\"level\":17,\"match_duration\":1775,\"last_hits\":61,\"gold\":2862,\"assists\":10}]"

var jsonNSString = getJSON("lina_final.json")

let parser = JSONParser()
let dotaArray = parser.parseJSONToArray(jsonNSString as String)
var treinamento: [Instancia] = []
var cont = 0
for dotaDict in dotaArray {
    println("\(cont)")
    cont++
    var instancia: Instancia = parser.parseDotaDictToInstancia(dotaDict as! Dictionary<String, Int>)
    if instancia.classe == 1 {
        println("Carry Found")
    }
    treinamento.append(instancia)
}

let percetron = Perceptron()

percetron.treinar(treinamento)
var total = 0.0
var acerto = 0.0
var erro = 0.0

for (_,instancia) in enumerate(treinamento) {
    total++
    if instancia.classe == percetron.classificar(instancia) {
        acerto++
    } else {
        erro++
    }
    println("\(instancia.classe) -> \(percetron.classificar(instancia))")
}

println("acerto: \(acerto/total * 100)%")
println("erro: \(erro/total * 100)%")
