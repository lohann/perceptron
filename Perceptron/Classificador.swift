//
//  Classificador.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 08/04/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

import Cocoa

protocol Classificador {
    func treinar(train: Array<Instancia>)
    func classificar(instancia: Instancia) -> Double
}
