//
//  Dendrito.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 06/05/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//
import Cocoa

class Dendrito {
    var peso:Double
    
    init() {
        peso = 0
    }
    
    init(peso:Double) {
        self.peso = peso
    }
}
