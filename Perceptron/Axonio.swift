//
//  Axonio.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 06/05/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//
import Cocoa

class Axonio {
    var funcaoDeSaida:Int
    
    init() {
        self.funcaoDeSaida = 0
    }
    
    func calcular(delta:Double) -> Double {
        var calculo = 0.0
        
        switch funcaoDeSaida {
        case 0:
            calculo = delta > 0 ? 1 : 0
        case 1:
            calculo = 1 / (1 + exp(-delta));
        default:
            calculo = delta;
        }
        
        return calculo
    }
}
