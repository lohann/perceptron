//
//  Perceptron.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 08/04/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

import Cocoa

class Perceptron: Classificador {
    
    private var dendritos = Array<Dendrito>()
    private var bias = 0.0
    
    private var maxGeracoes = 50
    private var minErro = 0.0
    private var taxaDeAprendizagem = 0.1
    
    private let axonio:Axonio = Axonio()
    
    func treinar(train: Array<Instancia>) {
        var numeroDePesos:Int = train[0].valores.count
        
        // Inicializa o array
        dendritos = Array<Dendrito>(count: numeroDePesos, repeatedValue: Dendrito(peso:0.0))
        
        // Sorteia numeros aleatórios entre 0 e 1 para os pesos
        for (index,_) in enumerate(dendritos) {
            dendritos[index] = Dendrito(peso:1.0)
            //dendritos[index].peso = 1
            dendritos[index].peso = Double(arc4random()%10000) / Double(10000)
        }
        
        // Aplica a regra Percetron
        var contagemGeracoes = 0
        var erro = 1.0
        var delta = 0.0
        var saida = 0.0
        var valorAlvo = 0.0
        
        while erro > minErro && contagemGeracoes < maxGeracoes { // critério de parada
            erro = 0
            for (index, instancia) in enumerate(train) {
                
                valorAlvo = instancia.classe
                saida = calcularSaida(instancia)
                delta = taxaDeAprendizagem * (valorAlvo - saida)
                
                // --- Impressao da entrada e dos pesos
                for (index,_) in enumerate(instancia.valores) {
                    print("v\(index+1) = \(instancia.valores[index]) ")
                }
                println()
                var sum = calcularSoma(instancia)
                imprimirPesos();
                println("index: \(index+1)")
                println("  sum: \(sum)")
                println(" real: \(valorAlvo)")
                println("saida: \(saida)")
                println()
                // --- Fim impressao
                
                atualizarPesos(instancia, delta: delta)
                
                if delta != 0 {
                    erro++
                }
            }
            println("\n------------------------\n")
            contagemGeracoes++;
            erro = erro / Double(train.count)
            
            if erro == 0 {
                break
            }
        }
    }
    
    func classificar(instancia: Instancia) -> Double{
        return calcularSaida(instancia)
    }
    
    private func atualizarPesos(instancia: Instancia, delta: Double) {
        for (index, dendrito) in enumerate(dendritos) {
            dendrito.peso += instancia.valores[index] * delta
        }
        bias += delta
    }
    
    private func calcularSoma(instancia: Instancia) -> Double {
        var saida = 0.0
        for (index,_) in enumerate(dendritos) {
            saida += instancia.valores[index] * dendritos[index].peso
        }
        saida += bias
        return saida
    }
    
    private func calcularSaida(instancia: Instancia) -> Double {
        var saida = calcularSoma(instancia)
        return axonio.calcular(saida)
    }
    
    private func imprimirPesos() {
        for (index,_) in enumerate(dendritos) {
            print("x\(index+1) = \(dendritos[index].peso) ")
        }
        println("bias:\(bias)")
    }
    
}
