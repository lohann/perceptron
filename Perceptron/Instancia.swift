//
//  Instancia.swift
//  Perceptron
//
//  Created by Lohann Paterno Coutinho Ferreira on 08/04/15.
//  Copyright (c) 2015 Lohann Paterno Coutinho Ferreira. All rights reserved.
//

import Cocoa

class Instancia {
    let valores: Array<Double>
    let classe: Double
    
    init(valores: Array<Double>, classe:Double) {
        self.valores = valores
        self.classe = classe
    }
}
